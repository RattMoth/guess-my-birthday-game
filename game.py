player_name = input("Hi! What's your name?\n")
month_high = 12
month_low = 1
year_high = 2004
year_low = 1924

months = ["Jan", "Feb", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

for attempt in range(1, 5):
    # Booleans to determine if a new year/month needs to be calculated
    correct_year = False
    correct_month = False

    # Sorry for the double negatives
    if not correct_year:
        year_average = int((year_high + year_low) / 2)
    if not correct_month:
        month_average = int((month_high + month_low) / 2)

    print(f"Guess number {attempt}: {player_name}, were you born in {months[month_average - 1]} {year_average}?")

    answer = input("Yes or no?\n")

    if answer == "yes":
        print("I knew it!")
        exit()
    elif answer == "no":
        print("Drat! Lemme try again!")

        if not correct_month:
            month_prompt = input("Was the MONTH too high, too low, or correct? (Type 'high', 'low', or 'correct')\n")

            if month_prompt == "high":
                month_high = month_average
            elif month_prompt == "low":
                month_low = month_average
            elif month_prompt == "correct":
                correct_month = True

        if not correct_year:
            year_prompt = input("Was the YEAR too high, too low, or correct?\n")
            if year_prompt == "high":
                year_high = month_average
            elif year_prompt == "low":
                year_low = year_average
            elif year_prompt == "correct":
                correct_year = True

print("I have other things to do. Good bye.")
